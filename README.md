# sws-ripple-button

Ripple effect button with webcomponents.

## Usage

```html
<sws-ripple-button>
  Button Label
</sws-ripple-button>
```

```html
<sws-ripple-button>
  <span class="btn">Button Label</span>
</sws-ripple-button>
```

## License

MIT

## Author

iNo
