(function() {

  const polyfills = {
    'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.min.js': ('Reflect' in window) && ('Promise' in window),
    'https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/1.0.14/webcomponents-lite.js': ((!!HTMLElement.prototype.attachShadow) || ('customElements' in window))
  };

  const load = function(url, callback) {
    if (polyfills[url]) {
      if (callback) {
        callback();
      }
      return;
    }
    let sc = document.createElement('SCRIPT');
    sc.src = url;
    sc.onload = sc.onreadystatechange = function() {
      if (callback) {
        callback();
      }
    };
    const tag = document.querySelector('script');
    tag.parentNode.insertBefore(sc, tag);
  };

  load(
    'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.min.js',
    function() {
      load(
        'https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/1.0.14/webcomponents-lite.js',
        function() {
          load('./sws-ripple-button.min.js');
        }
      );
    }
  );
})();
