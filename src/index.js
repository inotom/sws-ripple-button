const SwsRippleButton = function() {
  return Reflect.construct(HTMLElement, [], SwsRippleButton);
};

SwsRippleButton.prototype = Object.create(HTMLElement.prototype);

SwsRippleButton.prototype.constructor = SwsRippleButton;

SwsRippleButton.prototype.connectedCallback = function() {

  this.attachShadow({
    mode: 'open'
  }).innerHTML = SwsRippleButton.template;

  const btn = this.shadowRoot.querySelector('.sws-ripple-btn');
  const ripple = this.shadowRoot.querySelector('.sws-ripple-btn__ripple');

  btn.addEventListener('click', function(e) {
    const rect = btn.getBoundingClientRect();
    const size = (btn.clientWidth > btn.clientHeight ? btn.clientWidth : btn.clientHeight) * 2;
    ripple.style.width = size + 'px';
    ripple.style.height = size + 'px';
    ripple.style.left = (e.clientX - rect.left - size / 2) + 'px';
    ripple.style.top = (e.clientY - rect.top - size / 2) + 'px';
    btn.setAttribute('disabled', 'disabled');
    if (!ripple.classList.contains('sws-ripple-btn__ripple--animate')) {
      ripple.setAttribute('class', 'sws-ripple-btn__ripple--animate');
      setTimeout(() => {
        btn.removeAttribute('disabled');
        ripple.setAttribute('class', 'sws-ripple-btn__ripple');
      }, 750);
    }
  });
};

SwsRippleButton.template = `
<style>
.sws-ripple-btn {
  overflow: hidden;
  display: inline-block;
  position: relative;
}
.sws-ripple-btn__ripple--animate,
.sws-ripple-btn__ripple {
  position: absolute;
  background-color: rgba(0, 0, 0, .3);
  border-radius: 50%;
  pointer-events: none;
  transform: scale(0);
  opacity: 0;
}
.sws-ripple-btn__ripple--animate {
  animation: sws-ripple-btn-animation .75s ease-out;
}
@keyframes sws-ripple-btn-animation {
  from {
    opacity: 1;
  }
  to {
    transform: scale(2);
    opacity: 0;
  }
}
</style>
<div class="sws-ripple-btn" role="button">
  <div class="sws-ripple-btn__ripple"></div>
  <slot></slot>
</div>
`;

export default SwsRippleButton;
